//
//  MobileChallengeTests.swift
//  MobileChallengeTests
//
//  Created by Iberkrhdz- on 18/04/2020.
//  Copyright © 2020 Tabaiba Software Solutions. All rights reserved.
//

import XCTest
@testable import MobileChallenge

class MobileChallengeTests: XCTestCase {
    var mainlist: MainListViewController? = nil
    
    override func setUpWithError() throws {
        self.mainlist = MainListViewController()
    }

    override func tearDownWithError() throws {}

    func testGetMoviesRequest() throws {
        let promise = expectation(description: "Status code: 200")

        XCTAssertEqual(self.mainlist!.movies?.count, 0, "Movies count should be 0 at start")
                
        let headers = self.mainlist!.prepareRequestGetMoviees()
        MovieRequest.get(headers: headers, searchText: "thor", completion: { isRequestSuccess, movies, response in
            if isRequestSuccess {
                self.mainlist!.movies = movies["results"]
            }
            promise.fulfill()
        })
        wait(for: [promise], timeout: 5)

        XCTAssertGreaterThan(self.mainlist!.movies!.count, 0, "Movies count should be greater than 0 after getmovies() call")
    }

}
