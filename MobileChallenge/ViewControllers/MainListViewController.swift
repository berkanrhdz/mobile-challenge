//
//  MainListViewController.swift
//  MobileChallenge
//
//  Created by Iberkrhdz- on 19/04/2020.
//  Copyright © 2020 Tabaiba Software Solutions. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class MainListViewController: UIViewController {
    @IBOutlet weak var moviesTV: UITableView!
    @IBOutlet weak var searchTF: TextFieldInset!
    @IBOutlet weak var searchBTN: UIButton!
    
    var indicator = UIActivityIndicatorView()
    var indicatorV: UIView!
    var movies: JSON? = [:]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configDelegates()
        
        self.hideKeyboardWhenTappedAround()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    
    @IBAction func searchBTNAction(_ sender: Any) {
        self.activityIndicator()
        self.indicator.startAnimating()
        
        self.getMovies(searchtext: self.searchTF.text!)
    }
    
    
    func configDelegates() {
        self.moviesTV.delegate = self
        self.moviesTV.dataSource = self
        
        self.searchTF.delegate = self
    }
    
    func getMovies(searchtext: String) {
        let headers = self.prepareRequestGetMoviees()
        
        MovieRequest.get(headers: headers, searchText: searchtext, completion: { isRequestSuccess, movies, response in
            if isRequestSuccess {
                self.movies = movies["results"]
                
                self.indicatorV.removeFromSuperview()
                self.moviesTV.reloadData()
            } else {
                self.indicatorV.removeFromSuperview()
            }
        })
    }
    
    func activityIndicator() {
        self.indicatorV = UIView(frame: self.moviesTV.bounds)
        self.indicatorV.backgroundColor = UIColor(named: "PrimaryBackgroundColor")
        
        self.indicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 80, height: 80))
        self.indicator.style = UIActivityIndicatorView.Style.large
        self.indicator.center = self.indicatorV.center
        self.indicator.color = UIColor(named: "PrimaryTextColor")
        
        self.indicatorV.addSubview(indicator)
        self.moviesTV.addSubview(indicatorV)
    }
    
    func prepareRequestGetMoviees() -> HTTPHeaders {
        let headers: HTTPHeaders = [
            "Accept": "application/json",
            "Content-Type": "application/json",
        ]
        
        return headers
    }
}


class MoviesTVCell: UITableViewCell {
    @IBOutlet weak var noImageLBL: UILabel!
    @IBOutlet weak var noImageLBL2: UILabel!
    @IBOutlet weak var movieIMGV: UIImageView!
    @IBOutlet weak var movieTitleLBL: UILabel!
    @IBOutlet weak var movieYearLBL: UILabel!
    @IBOutlet weak var movieVotesAverageLBL: UILabel!
}
