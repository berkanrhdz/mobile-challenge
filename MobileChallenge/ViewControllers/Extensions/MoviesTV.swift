//
//  MoviesTV.swift
//  MobileChallenge
//
//  Created by Iberkrhdz- on 19/04/2020.
//  Copyright © 2020 Tabaiba Software Solutions. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher

extension MainListViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.movies?.count == 0 {
            let emptyLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.view.bounds.size.height))
            emptyLabel.text = "No movies available"
            emptyLabel.textAlignment = NSTextAlignment.center
            
            self.moviesTV.backgroundView = emptyLabel
            
            return 0
        } else {
            self.moviesTV.backgroundView = nil
            
            return self.movies!.count
        }        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.moviesTV.dequeueReusableCell(withIdentifier: "MoviesTVCellID", for: indexPath) as! MoviesTVCell
        let movie = self.movies![indexPath.row]
        let url = URL(string: "https://image.tmdb.org/t/p/w1280/\(movie["poster_path"].stringValue)")
       
        if movie["poster_path"].stringValue == "" {
            cell.noImageLBL.isHidden = false
            cell.noImageLBL2.isHidden = false
        } else {
            cell.noImageLBL.isHidden = true
            cell.noImageLBL2.isHidden = true
        }
        
        cell.movieIMGV.kf.setImage(with: url)
        cell.movieTitleLBL.text = movie["title"].stringValue
        cell.movieYearLBL.text = "\(movie["release_date"].stringValue.prefix(4))"
        cell.movieVotesAverageLBL.text = "\(movie["vote_average"].floatValue)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let movie = self.movies![indexPath.row]

        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "DetailsViewControllerID") as DetailsViewController
        vc.movie = movie
        
        self.present(vc, animated: true, completion: nil)
    }
}
