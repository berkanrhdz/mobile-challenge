//
//  DetailsViewController.swift
//  MobileChallenge
//
//  Created by Iberkrhdz- on 19/04/2020.
//  Copyright © 2020 Tabaiba Software Solutions. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON
import Kingfisher

class DetailsViewController: UIViewController {
    @IBOutlet weak var moviePosterIMGV: UIImageView!
    @IBOutlet weak var moviePosterV: UIView!
    @IBOutlet weak var movieTitleLBL: UILabel!
    @IBOutlet weak var movieDateLBL: UILabel!
    @IBOutlet weak var movieVotesAverageLBL: UILabel!
    @IBOutlet weak var movieOverviewLBL: UILabel!
    
    var movie: JSON? = [:]
    var moviePoster: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.configViews()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.setData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
    }
    
    
    @IBAction func backBTNAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func configViews() {
        let tintV = UIView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: self.moviePosterV.bounds.size.height))
        tintV.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        
        self.moviePosterIMGV.addSubview(tintV)
        
        self.moviePosterV.layer.shadowColor = UIColor.black.cgColor
        self.moviePosterV.layer.shadowOpacity = 1
        self.moviePosterV.layer.shadowOffset = .init(width: 0, height: -4)
        self.moviePosterV.layer.shadowRadius = 10
    }
    
    func setData() {
        let url = URL(string: "https://image.tmdb.org/t/p/w1280/\(self.movie!["poster_path"].stringValue)")
        
        self.moviePosterIMGV.kf.setImage(with: url)
        self.movieTitleLBL.text = self.movie!["title"].stringValue
        self.movieDateLBL.text = "\(self.transformDate(dateString: self.movie!["release_date"].stringValue))"
        self.movieVotesAverageLBL.text = "\(self.movie!["vote_average"].floatValue)"
        
        if self.movie!["overview"].stringValue != "" {
            self.movieOverviewLBL.text = self.movie!["overview"].stringValue
        }
    }
    
    func transformDate(dateString: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy/MM/dd"
        let dateaux = formatter.date(from: dateString)

        formatter.dateFormat = "MMM dd, yyyy"
        let stringFormatted = formatter.string(from: dateaux!)

        return stringFormatted
    }
}
