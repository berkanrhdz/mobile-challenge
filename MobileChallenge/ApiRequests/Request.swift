//
//  Requests.swift
//  MobileChallenge
//
//  Created by Iberkrhdz- on 19/04/2020.
//  Copyright © 2020 Tabaiba Software Solutions. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

class Request {
    static var manager: Session?
    static var SERVERURL = "https://api.themoviedb.org/3/search/movie?api_key=3aa2122dc9eafa4f573fb5e7fda09a97&"

    init(timeoutRequest: Double, timeoutResource: Double) {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = timeoutRequest
        configuration.timeoutIntervalForResource = timeoutResource

        Request.manager = Alamofire.Session(configuration: configuration)
    }
}
