//
//  MovieRequest.swift
//  MobileChallenge
//
//  Created by Iberkrhdz- on 19/04/2020.
//  Copyright © 2020 Tabaiba Software Solutions. All rights reserved.
//

import Foundation
import UIKit
import Alamofire
import SwiftyJSON

class MovieRequest {
    static func get(headers: HTTPHeaders, searchText: String, completion: @escaping (_ isSuccess: Bool, _ responsebody: JSON, _ response: AFDataResponse<Any>) -> ()) {
        let url = "\(Request.SERVERURL)language=es-ES&query=\(searchText)&page=1&include_adult=false"

        _ = Request(timeoutRequest: 5.0, timeoutResource: 5.0)
        Request.manager!.request(url, method: .get, encoding: JSONEncoding.default, headers: headers).responseJSON { response in
            switch(response.result) {
                case .success(_):
                    let json = try! JSON(data: response.data!)
                    completion(true, json, response)
                    
                    break
                case .failure(_):
                    completion(false, [:], response)

                    break
            }
        }
    }
}



