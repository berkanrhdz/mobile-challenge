//
//  CustomTextField.swift
//  MobileChallenge
//
//  Created by Iberkrhdz- on 19/04/2020.
//  Copyright © 2020 Tabaiba Software Solutions. All rights reserved.
//

import Foundation
import UIKit

class TextFieldInset: UITextField {
    let padding = UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 5)

    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }

    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
}
