//
//  MobileChallengeUITests.swift
//  MobileChallengeUITests
//
//  Created by Iberkrhdz- on 18/04/2020.
//  Copyright © 2020 Tabaiba Software Solutions. All rights reserved.
//

import XCTest

class MobileChallengeUITests: XCTestCase {
    let app = XCUIApplication()

    override func setUpWithError() throws {
        app.launch()
        
        continueAfterFailure = false
    }

    override func tearDownWithError() throws {}
    
    func testFirstViewLaunched() {
        XCTAssertTrue(app.otherElements["MainListView"].exists, "FirstView is MainListView")
    }
    
    func testCanPresentSecondView() {
        let searchTF = app.textFields["Search by name"]
        let searchBTN = app.buttons["magnifyingglass"]
        
        searchTF.tap()
        searchTF.typeText("thor")
        searchBTN.tap()
        
        if app.tables.cells.count > 0 {
            app.tables.cells.element(boundBy:0).tap()
            XCTAssertTrue(app.otherElements["DetailsView"].exists, "DetailsView is presented")
        }
    }
    
    func testCanBackToFirstView() {
        let searchTF = app.textFields["Search by name"]
        let searchBTN = app.buttons["magnifyingglass"]
        
        searchTF.tap()
        searchTF.typeText("thor")
        searchBTN.tap()
        
        if app.tables.cells.count > 0 {
            app.tables.cells.element(boundBy:0).tap()
            
            let backBTN = app.buttons["backBTN"]
            backBTN.tap()
            
            XCTAssertTrue(app.otherElements["MainListView"].exists, "FirstView is presented again")
        }
    }
}
